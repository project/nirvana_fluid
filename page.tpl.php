<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language ?>" xml:lang="<?php print $language ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>

  <?php print $styles; ?>
  <!--[if lte IE 6]>
    <link type="text/css" rel="stylesheet" media="all" href="<?php print $base_path . $directory; ?>/style-ie6.css" />
  <![endif]-->
  <!--[if IE 7]>
    <link type="text/css" rel="stylesheet" media="all" href="<?php print $base_path . $directory; ?>/style-ie7.css" />
  <![endif]-->

  <?php print $scripts; ?>
  <!--[if lte IE 6]>
    <script defer type="text/javascript" src="<?php print $base_path . $directory; ?>/js/pngfix.js"></script>
  <![endif]-->
  <!--[if IE]>
    <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?></script>
  <![endif]-->
</head>

<body class="<?php print $body_classes; ?>">
<div id="outer">

<div id="page">

  <?php if ($header) { ?>
    <div id="header-region"><?php print $header; ?></div>
  <?php } ?>

  <div id="header">
    <?php if ($logo) { ?>
      <a href="<?php print $base_path; ?>" title="<?php print t('Home'); ?>"><img id="logo" src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
    <?php } ?>
  
    <div id="header-text">
      <?php if ($site_name) { ?>
        <div id="site-name"><a href="<?php print $base_path ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a></div>
      <?php } ?>
  
      <?php if ($site_slogan) { ?>
        <div id="site-slogan"><?php print $site_slogan; ?></div>
      <?php } ?>
    </div><!-- /#header-text -->
  
    <?php if ($search_box) { ?>
      <div id="search-wrapper"><?php print $search_box; ?></div>
    <?php } ?>
  
    <div class="clear"></div>
  </div><!-- /#header -->
  
  <div id="navigation" class="menu<?php if ($primary_links) print " withprimary"; if ($secondary_links) print " withsecondary"; ?>">
    <?php if ($primary_links) { ?>
      <div id="primary" class="clear-block"><?php print theme('menu_links', $primary_links); ?></div>
    <?php } ?>
  
    <?php if ($secondary_links) { ?>
      <div id="secondary" class="clear-block"><?php print theme('menu_links', $secondary_links); ?></div>
    <?php } ?>
  </div><!-- /#navigation -->

  <div id="columns-wrapper">

    <div id="main" class="column">
      <?php if ($breadcrumb) print $breadcrumb; ?>
      <?php if ($mission) { ?><div id="mission"><?php print $mission; ?></div><?php } ?>
  
      <?php if ($content_above) { ?><div id="content-above"><?php print $content_above; ?></div><?php } ?>
  
      <?php if ($title) { ?><h1 id="page-title"><?php print $title; ?></h1><?php } ?>
      <?php if ($tabs) { ?><div class="tabs"><?php print $tabs; ?></div><?php } ?>
  
      <div id="main-inner">
        <?php print $messages; ?>
        <?php print $help; ?>
        <?php print $content; ?>
        <?php if ($feed_icons) { ?><div id="feed-icons"><?php print $feed_icons; ?></div><?php } ?>
      </div><!-- /#main-inner -->
  
      <?php if ($content_below) { ?><div id="content-below"><?php print $content_below; ?></div><?php } ?>
    </div><!-- /#main -->
  
    <?php if ($sidebar_left) { ?>
      <div id="left-sidebar" class="sidebar column"><div id="left-sidebar-inner">
        <?php print $sidebar_left; ?>
      </div></div><!-- /#left-sidebar-inner /#left-sidebar -->
    <?php } ?>
  
    <?php if ($sidebar_right) { ?>
      <div id="right-sidebar" class="sidebar column"><div id="right-sidebar-inner">
        <?php print $sidebar_right; ?>
      </div></div><!-- /#right-sidebar-inner /#right-sidebar -->
    <?php } ?>

  </div><!-- /#columns-wrapper -->

</div><!-- /#page -->

<div id="clearfooter"></div>
<div id="footer-wrapper">

  <div id="footer">
    <?php if ($footer_message) { ?><div id="footer-message"><?php print $footer_message; ?></div><!-- /#footer-message --><?php } ?>
    <?php if ($footer_region) { ?><div id="footer-region"><?php print $footer_region; ?></div><!-- /#footer-region --><?php } ?>
  </div><!-- /#footer -->

</div><!-- /#footer-wrapper -->

</div><!-- /#outer -->

<?php    /*  _____ ______ __  __ ______
         *  / ___// __  // / / // __  /
        *  / /_  / / / // / / // /_/ /            Four Kitchens  *
       *  / __/ / /_/ // /_/ // _  _/     We make BIG websites  *
      *  /_/   /_____//_____//_/ \_\  http://fourkitchens.com  *
     *  __  __  __ _______ _____ __   __ _____ __  __ _____   *
    *  / /_/ / / //__  __// ___// /__/ // ___//  \/ // ___/  *
   *  /   __/ / /   / /  / /   / ___  // __/ / /\  // /__   *
  *  / /\ \  / /   / /  / /__ / /  / // ___ / / / /___  /  *
 *  /_/  \_\/_/   /_/  /____//_/  /_//____//_/ /_//____/  */ ?>

<?php print $closure; ?>

</body>
</html>
