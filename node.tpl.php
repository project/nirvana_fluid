<div class="<?php print $node_classes; ?>" id="node-<?php print $node->nid; ?>">
  <?php if ($page == 0) { ?>
    <h2 class="title"><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php } ?>

  <?php if ($submitted) { ?>
    <div class="submitted"><?php print format_date($node->created, 'custom', 'F j, Y') . ' | ' . theme('username', $node); ?></div>
  <?php } ?>

  <div class="node-inner">
    <?php if ($picture) print $picture; ?>

    <div class="content">
      <?php print $content; ?>

      <?php if (count($taxonomy)) { ?>
        <div class="taxonomy"><?php print t('Posted in ') . $terms; ?></div>
      <?php } ?>
    </div>

    <div class="clear"></div><!-- clears floated picture (avatar) -->

    <?php if ($node->links) { ?><div class="links"><?php print nirvana_fluid_custom_links($node->links, 'post'); ?></div><?php } ?>
  </div><!-- /node-inner -->
</div><!-- /node -->
