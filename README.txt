          _____ ______ __  __ ______
         / ___// __  // / / // __  /
        / /_  / / / // / / // /_/ /            Four Kitchens
       / __/ / /_/ // /_/ // _  _/     We make BIG websites
      /_/   /_____//_____//_/ \_\  http://fourkitchens.com
     __  __  __ _______ _____ __   __ _____ __  __ _____
    / /_/ / / //__  __// ___// /__/ // ___//  \/ // ___/
   /   __/ / /   / /  / /   / ___  // __/ / /\  // /__
  / /\ \  / /   / /  / /__ / /  / // ___ / / / /___  /
 /_/  \_\/_/   /_/  /____//_/  /_//____//_/ /_//____/


***********************************************************
* Nirvana: Enlightened Theming for Drupal                 *
***********************************************************

Thank you for downloading Nirvana.


=====
Licensing
-----

Icons 
-----
Colored bullet icons are original works created by Four Kitchens.  The other icons are part of the Mini Icon Set (http://www.famfamfam.com/lab/icons/mini/). Per the creator's "About" page (http://www.famfamfam.com/about/), these icons are public domain.


PNG fix (pngfix.js)
-----
From the author's website (http://homepage.ntlworld.com/bobosola/index.htm): "There is no official licence of any kind on this -- it's simply freeware. Use it or change it as you wish."
