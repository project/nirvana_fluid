<div class="<?php print $comment_classes; ?>">
  <?php if ($new) { ?><div class="new"><?php print $new; ?></div><?php } ?>
  <?php if ($title) { ?><h3 class="title"><?php print $title; ?></h3><?php } ?>
  <div class="submitted"><?php print format_date($comment->timestamp, 'custom', 'F j, Y') . ' | ' . theme('username', $comment); ?></div>
  <?php if ($picture) print $picture; ?>
  <div class="content"><?php print $content; ?></div>
  <div class="clear"></div>
  <?php if ($links) { ?><div class="links"><?php print $links; ?></div><?php } ?>
</div><!-- /comment -->
