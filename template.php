<?php

/**
 * @file template.php
 * Theme overrides for Nirvana Fluid.
 * 
 * ABOUT
 * 
 * Nirvana Fluid was inspired by and based on Lullabot's Zen theme. (You may notice 
 * several similarities.) The purpose of Nirvana Fluid is to provide a "themer's 
 * theme" -- a theme upon which designers and developers can build and 
 * customize their site's output and appearance -- without employing any
 * "sub-theming." (Zen, for example, in intended to be "sub-themed" in order
 * to achieve a custom look and feel.)
 * 
 * Some of the functions provided in this file are probably too complicated 
 * for beginning themers. We've included them simply to illustrate how much 
 * control you have over Drupal's output. Other functions provided much-needed 
 * CSS classes to help designers style output.
 * 
 * - Four Kitchens | http://fourkitchens.com
 * 
 * ABOUT NIRVANA'S TEMPLATE.PHP
 * 
 * This file lets you add new regions for block content, override Drupal's 
 * theme functions, intercept or make additional variables available to your 
 * theme, and create custom PHP logic. For more information, visit the Theme 
 * Developer's Guide on Drupal.org: http://drupal.org/node/509
 */


/*
 * MODIFYING OR CREATING REGIONS
 * 
 * Regions are areas in your theme where you can place blocks. Typically, most 
 * themes use the following regions: "left sidebar", "right sidebar", "header", 
 * and "footer". These are so common, in fact, that some of them are built into 
 * PHPTemplate (the engine that processes this theme). You can, however, 
 * declare as many regions as you want. Once declared, they are made available 
 * to the page.tpl.php file as a variable that you can print. (For example, the 
 * region "header" can be outputted using "print $header".)
 * 
 * You can choose the regions in which blocks should be placed by go to 
 * administer > site building > blocks. New regions you define here will
 * automatically show up in the drop-down list by their human readable name.
 */

/**
 * Declare the available regions implemented by the PHPTemplate engine.
 * 
 * @return
 *   An array of regions. The first array element will be used as the default 
 *   region for themes. Each array element takes the format: 
 *   variable_name => t('human readable name')
 * 
 * This function is a copy of nirvana_fluid_regions(). Only two regions have been 
 * added: content_above and content_below.
 * 
 * The variable names for the content inside of regions is set in the 
 * nirvana_fluid_page() function. Here's how the regions listed below are called 
 * in page.tpl.php:
 *   left          --> $sidebar_left
 *   right         --> $sidebar_right
 *   content_above --> $content_above
 *   content       --> $content
 *   content_below --> $content_below
 *   header        --> $header
 *   footer_region --> $footer_region
 * 
 * Note that the footer is called "footer_region". This is done to prevent 
 * PHPTemplate from combining the site-wide footer message defined at 
 * Admin > Site Configuration > Site Information with the blocks contained in 
 * the footer region. This allows greater control of the theming of the footer 
 * message as it relates to the blocks contain in the footer region. (For 
 * hardcore themers, you can also try overriding the nirvana_fluid_page() 
 * function to achieve similar results.)
 */
function nirvana_fluid_regions() {
  return array(
       'left' => t('left sidebar'),
       'right' => t('right sidebar'),
       'content_above' => t('content top'), // added by Nirvana Fluid
       'content' => t('content'),
       'content_below' => t('content bottom'), // added by Nirvana Fluid
       'header' => t('header'),
       'footer_region' => t('footer'),
  );
}


/*
 * OVERRIDING THEME FUNCTIONS
 *
 * The Drupal theme system uses special theme functions to generate HTML 
 * output. To customize HTMl output, we have to override the theme function 
 * that generates it. To override a theme function, find the original 
 * function in Drupal's API (http://api.drupal.org), paste it here, and change 
 * the function's prefix from theme_ to nirvana_fluid_.  For example:
 * 
 *   original:         theme_breadcrumb() 
 *   theme override:   nirvana_fluid_breadcrumb()
 * 
 * In the following example, we override theme_breadcrumb() to change the 
 * separator links from "&raquo;"" (right-angle quote) to "::" (two colons).
 * More complicated examples follow.
 */

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function nirvana_fluid_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div id="breadcrumb">'. implode(' :: ', $breadcrumb) .'</div>' . "\n";
  }
}


/**
 * Add paragraph markup to single-item messages.
 */
function nirvana_fluid_status_messages($display = NULL) {
  $output = '';
  foreach (drupal_get_messages($display) as $type => $messages) {
    $output .= '<div class="messages ' . $type . '">' . "\n";
    if (count($messages) > 1) {
      $output .= '<ul>' . "\n";
      foreach ($messages as $message) {
        $output .= '<li>'. $message . '</li>' . "\n";
      }
      $output .= '</ul>' . "\n";
    }
    else {
      $output .= '<p>' . $messages[0] . '</p>';
      // $output .= $messages[0]; // original line
    }
    $output .= '</div><!-- /messages -->' . "\n";
  }
  return $output;
}


/**
 * Add classes "first" and "last" to list items (li) in primary and secondary menus.
 */
function nirvana_fluid_menu_links($links) {
  $output = '';

  if (!empty($links)) {
    $level_tmp = explode('-', key($links));
    $level = $level_tmp[0];
    $num_links = count($links);
    $current_link = 1;

    $output = '<ul class="links-' . $level . '">' . "\n";

    foreach ($links as $index => $link) {
      $li_classes = array();

      if ($current_link == 1) {
        $li_classes[] = 'first';
      }
      if ($current_link == $num_links) {
        $li_classes[] = 'last';
      }
      if (stripos($index, 'active') !== FALSE) {
        $li_classes[] = 'active';
      }

      $output .= '<li' . drupal_attributes(array('class' => implode(' ', $li_classes))) . '>';
      $output .= l($link['title'], $link['href'], $link['attributes'], $link['query'], $link['fragment']);
      $output .= '</li>' . "\n";

      $current_link++;
    }

    $output .= '</ul>' . "\n";
  }

  return $output;
}


/**
 * Add classes "first," "last," or "only" to list items (li) within menu blocks.
 */
function nirvana_fluid_menu_tree($pid = 1) {
  if ($tree = nirvana_fluid_menu_tree_improved($pid)) {
    return '<ul class="menu">' . "\n". $tree ."\n" . '</ul>' . "\n";
  }
}

function nirvana_fluid_menu_tree_improved($pid = 1) {
  $menu = menu_get_menu();
  $output = '';

  if (isset($menu['visible'][$pid]) && $menu['visible'][$pid]['children']) {
    $num_children = count($menu['visible'][$pid]['children']);
    for ($i=0; $i < $num_children; ++$i) {
      $mid = $menu['visible'][$pid]['children'][$i];
      $type = isset($menu['visible'][$mid]['type']) ? $menu['visible'][$mid]['type'] : NULL;
      $children = isset($menu['visible'][$mid]['children']) ? $menu['visible'][$mid]['children'] : NULL;

      $class = '';
      if ($i == 0) {
        // Item is first
        $class = 'first';
      }
      else if ($i == $num_children - 1) {
        // Item is last
        $class = 'last';
      }
      else if ($num_children - 1 == 0) {
        // Single-item menu (item is both first and last)
        $class = 'only';
      }

      $output .= theme('menu_item', $mid, menu_in_active_trail($mid) || ($type & MENU_EXPANDED) ? theme('menu_tree', $mid) : '', count($children) == 0, $class);     
    }
  }

  return $output;
}

function nirvana_fluid_menu_item($mid, $children = '', $leaf = TRUE, $extraclass = '') {
  return '<li class="'. ($leaf ? 'leaf' : ($children ? 'expanded' : 'collapsed')) . ($extraclass ? ' ' . $extraclass : '') . '">'. menu_item_link($mid, TRUE, $extraclass) . $children ."</li>\n";
}


/**
 * Return customized links for a node or comment
 *
 * @param $links
 *   Links for a node/comment
 */
function nirvana_fluid_custom_links($links, $type='page') {

  // change the text of some links
  if ($links['comment_add'] != '') {
    $links['comment_add']['title'] = t('Reply to this @type', array('@type' => $type));
  }
  if ($links['comment_reply'] != '') {
    $links['comment_reply']['title'] = t('Reply to this comment');
  }

  // Forward module
  if ($links['forward_links'] != '') {
    $links['forward_links']['title'] = t('E-mail this @type to a friend', array('@type' => $type));
  }

  return theme('links', $links, array('class' => 'links inline'));
}



/**
 * Add a better wrapper around entire comment section
 */
function nirvana_fluid_comment_wrapper($content) {
  $output = '';

  $has_comments = FALSE;
  if (arg(0) == 'node' && is_numeric(arg(1)) && !arg(2)) {
    $node = node_load(arg(1));
    if ($node->comment_count > 0) {
      $has_comments = TRUE;
    }
  }

  // if the node has no comments, nothing will output
  if ($content) {
    $output .= '<div id="comments">' . "\n";

    if ($has_comments) {
      $output .= '<h2 class="title">' . t('Comments') . '</h2>' . "\n";
    }

    $output .= $content . "\n";
    $output .= '</div><!-- /#comments -->' . "\n";
  }

  return $output;
}


/**
 * Override theme_page() to preserve the display of blocks on error pages.
 * 
 * Useful links:
 * http://drupal.org/node/129762#comment-232868
 * http://api.drupal.org/api/function/phptemplate_page/5
 */
function nirvana_fluid_page($content, $show_blocks = TRUE) {
  return phptemplate_page($content, TRUE);
}


/**
 * Determine roles using user_access().
 *
 * This method is superior than defining "admin" as a role name.
 *   Example of the inferior method: $nirvana_fluid_is_admin = in_array('admin', $user->roles);
 *
 * True "admin" access must be defined by whether they have access to certain tasks.
 * Here, we define an "admin" as one who has the ability to "administer site configuration."
 *
 * This function is used below in _phptemplate_variables().
 */
function nirvana_fluid_is_admin() {
  return user_access('administer site configuration');
}


/*
 * CREATE OR MODIFY VARIABLES FOR YOUR THEME
 *
 * The most powerful function available to themers is the _phptemplate_variables() function. It allows you
 * to pass newly created variables to different template (tpl.php) files in your theme. Or even unset ones you don't want
 * to use.
 *
 * It works by switching on the hook, or name of the theme function, such as:
 *   - page
 *   - node
 *   - comment
 *   - block
 *
 * By switching on this hook you can send different variables to page.tpl.php file, node.tpl.php
 * (and any other derivative node template file, like node-forum.tpl.php), comment.tpl.php, and block.tpl.php
 */

/**
 * Intercept template variables
 *
 * @param $hook
 *   The name of the theme function being executed
 * @param $vars
 *   A sequential array of variables passed to the theme function.
 */
function _phptemplate_variables($hook, $vars = array()) {
  // Get the currently logged in user
  global $user;

  switch ($hook) {

    case 'page':

      /*
       * Add body classes and variables for page.tpl.php
       * Allows advanced theming based on context (home page, node of certain type, etc.)
       */

      $body_classes = array();

      // Is this the front page?
      $body_classes[] = ($vars['is_front']) ? 'front' : 'not-front';

      // Is the user logged in?
      // An anonymous user has a user ID of zero
      if ($user->uid > 0) {
        // The user is logged in
        $vars['logged_in'] = TRUE;
        $body_classes[] = 'logged-in';
      }
      else {
        // The user is logged out
        $vars['logged_in'] = FALSE;
        $body_classes[] = 'not-logged-in';
      }

      // Set body class for number of columns in use
      if ($vars['sidebar_left'] && $vars['sidebar_right']) {
        $body_classes[] = 'both-sidebars';
      }
      else if ($vars['sidebar_left']) {
        $body_classes[] = 'left-sidebar';
      }
      else if ($vars['sidebar_right']) {
        $body_classes[] = 'right-sidebar';
      }
      else {
        $body_classes[] = 'no-sidebars';
      }

      // Determine admin access
      // An "admin" is defined in the nirvana_fluid_is_admin() function above
      if (nirvana_fluid_is_admin()) {
        $body_classes[] = 'admin';
      }

      // Reset value of footer message
      // Otherwise, nirvana_fluid_page() appends a line break and region output to this variable
      $vars['footer_message'] = filter_xss_admin(variable_get('site_footer', FALSE));

      // Set a body class for presence of footer message
      // This function checks for a single space (" ") because PHPTemplate adds one in 
      if (!empty($vars['footer_message'])) {
        $body_classes[] = 'with-footer-message';
      }

      // Implode with spaces
      $vars['body_classes'] = implode(' ', $body_classes);

      break;

    case 'node':

      /*
       * Add node classes
       */

      $node_classes = array('node');

      // Is the node unpublished?
      if (!$vars['node']->status) {
        $node_classes[] = 'node-unpublished';

        // Display a warning if this is a full node view (i.e., not a teaser or 
        // a page with several nodes) and the user is an admin.
        // An "admin" is defined in the nirvana_fluid_is_admin() function above
        if ($vars['page'] && nirvana_fluid_is_admin()) {
          drupal_set_message(t('This node is not published') .' ('. $vars['title'] .' [NID: '. $vars['nid'] .']).', 'warning');
        }
      }

      // Is the node stickied?
      if ($vars['sticky']) {
        $node_classes[] = 'sticky';
      }

      // Add a class for the node's content type
      $node_classes[] = 'ntype-'. nirvana_fluid_id_safe($vars['node']->type);

      // Implode with spaces
      $vars['node_classes'] = implode(' ', $node_classes);

      break;

    case 'comment':

      // Load the node that the current comment is attached to
      $node = node_load($vars['comment']->nid);

      /*
       * Add comment classes and variables for comment.tpl.php
       */

      $comment_classes = array('comment');

      // Is this comment new?
      if (!empty($vars['comment']->new)) {
        $vars['comment']->new = t('new');
        $comment_classes[] = 'comment-new';
      }

      // Is this comment published?
      if ($comment->status == COMMENT_NOT_PUBLISHED) {
        $comment_classes[] = 'comment-unpublished';
      }

      // Is this comment authored by the node's author?
      if ($vars['comment']->uid == $node->uid) {
        $comment_classes[] = 'comment-author';
        $vars['comment_author'] = TRUE;
      }
      else {
        $vars['comment_author'] = FALSE;
      }

      // Is this comment authored by the current user?
      if ($vars['comment']->uid == $user->uid) {
        $comment_classes[] = 'comment-mine';
        $vars['comment_mine'] = TRUE;
      }
      else {
        $vars['comment_mine'] = FALSE;
      }

      // Implode with spaces
      $vars['comment_classes'] = implode(' ', $comment_classes);

      break;
  }

  return $vars;
}


/**
 * Converts a string to a suitable html ID attribute.
 * - Prefixes string with 'n' if the initial character is numeric.
 * - Replaces non-alphanumeric characters with '-'.
 * - Converts entire string to lowercase.
 * - Works for classes too!
 * 
 * @param string $string
 *  the string
 * @return
 *  the converted string
 */
function nirvana_fluid_id_safe($string) {
  if (is_numeric($string{0})) {
    // if the first character is numeric, add 'n' in front
    $string = 'n' . $string;
  }
  return strtolower(preg_replace('/[^a-zA-Z0-9-]+/', '-', $string));
}
